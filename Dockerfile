FROM node:12-alpine

WORKDIR /app

COPY . .

RUN npm i

RUN npm run build

RUN npm run clean

CMD npm start
