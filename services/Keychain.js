import { Repeater } from '~/services/Repeater';

export class Keychain {

  static requestPrune = async (post) => {
    return new Promise((resolve, reject) => {
      if (!window.steem_keychain) {
        reject({message: 'You neeed to have Steem Keychain installed'});
      }
      window.steem_keychain.requestPost(post.author, post.title, post.body, post.parent_permlink, post.parent_author,
        {app: 'pruner', message: 'See you on the other side!'}, post.permlink, '', function (response) {
          response.success ? resolve(response) : reject(response)
        })
    })
  }

  static prune = async (post, repeat = 5) => new Repeater(repeat).execute(async () => {
    return await Keychain.requestPrune(post)
  });
}
