import { Repeater } from '~/services/Repeater';

const steem = require('steem');

steem.api.setOptions({url: 'https://api.steemit.com'});

export class Steem {
  static getPosts = async (username, onLoadHandler) => {

    let query = {tag: username, limit: 25};

    let count = 25;
    let start_permlink = null;
    let start_author = username;

    while (count >= 25) {
      if (start_permlink) {
        query.start_permlink = start_permlink;
        query.start_author = start_author;
      }

      const articles = await Steem.getFromBlockchainWithRepeat(query);
      const filtered = articles.filter(a => a.author === username);
      const posts = !start_permlink ? filtered : filtered.splice(1, articles.length - 1);

      onLoadHandler(posts)

      count = articles.length;

      if (articles.length === 25) {
        start_permlink = articles[articles.length - 1].permlink;
        start_author = articles[articles.length - 1].author;
      }

    }
  }

  static getFromBlockchainWithRepeat = async (query) => new Repeater().execute(async () => {
    return await steem.api.getDiscussionsByBlogAsync(query)
  })
}
