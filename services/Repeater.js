export class Repeater {

  constructor(repeat) {
    this.repeat = repeat || 3;
    this.count = 0;
  }

  async execute (handler) {
    try {
      return await handler();
    } catch (e) {
      this.count++;
      if (this.count < this.repeat) {
        console.error(e)
        return await this.execute(handler)
      } else {
        throw e;
      }
    }
  }
}
