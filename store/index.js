export const state = () => ({
  posts: [],
  username: '',
  apiChecked: false,
  ignoreErrors: true,
  removeContent: true,
  addCustomMessage: false,
  skipLastPost: false,
  numberOfPostsToSkip: 1,
  customMessage: '',
  pruning: false,
  method: 'move',
  frontend: 'https://hive.blog',
  inProgress: null
});
//
// const generateSetter = (field) => (state, value) => {
//   state[field] = value;
// }
//
// const fields = [
//   'ignoreErrors', 'addCustomMessage', 'removeContent', 'apiChecked', 'pruning', frontend
// ]

export const mutations = {
  // ...[fields.map(generateSetter)]
  setIgnoreErrors (state, value) {
    state.ignoreErrors = value;
  },
  setSkipLastPost (state, value) {
    state.skipLastPost = value;
  },
  setNumberOfPostsToSkip (state, value) {
    state.numberOfPostsToSkip = value;
  },
  setAddCustomMessage (state, value) {
    state.addCustomMessage = value;
  },
  setCustomMessage (state, value) {
    state.customMessage = value;
  },
  setRemoveContent (state, value) {
    state.removeContent = value;
  },
  setApiChecked (state, value) {
    state.apiChecked = value;
  },
  setPruning (state, value) {
    state.pruning = value;
  },
  setFrontend (state, value) {
    state.frontend = value;
  },
  setMethod (state, value) {
    state.method = value;
  },
  setUsername (state, username) {
    state.username = username;
  },
  addPosts(state, posts) {
    state.posts.push(...posts)
  },
  clearPosts (state) {
    state.posts = []
  },
  setInProgress (state, permlink) {
    state.inProgress = permlink
  },
  setMethod (state, method) {
    state.method = method;
  },
  updatePost (state, post) {
    state.posts = state.posts.map( p => {
      if (p.permlink === post.permlink) {
        p.title = post.title;
        p.body = post.body;
      }
      return p;
    })
  },
  setFrontend (state, frontend) {
    state.frontend = frontend;
  }
};
